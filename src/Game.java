import java.util.Random;
import java.util.Scanner;

public class Game {
    private int gameNumber;

    public Game() {
        Random random = new Random();
        gameNumber = random.nextInt(100);
    }

    public String getUserName () {
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();
        return name;
    }

    public int getUserNumber () {
        Scanner in = new Scanner(System.in);
        int userNumber = in.nextInt();
        return userNumber;
    }
    public void start () {
        final String SMALL_NUMBER_ANSWER = "Your number is too small. Please, try again..";
        final String BIGGER_NUMBER_ANSWER = "Your number is too big. Please, try again.";

        System.out.println("Name yourself");
        String name = getUserName();
        System.out.println("Let the game begin!");

        while(true) {
            int userNumber = getUserNumber();
            if(userNumber > gameNumber) {
                System.out.println(BIGGER_NUMBER_ANSWER);
            }
            if (userNumber == gameNumber) {
                System.out.println("Congratulations, " + name + "!");
            }
            if(userNumber < gameNumber) {
                System.out.println(SMALL_NUMBER_ANSWER);
            }
        }
    }
}
